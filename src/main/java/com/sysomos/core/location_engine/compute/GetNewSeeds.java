package com.sysomos.core.location_engine.compute;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sysomos.user.authority.utils.Pair;

public class GetNewSeeds {

	private final int scoreThreshold;
	private final double percentageThreshold;
	private final Map<Long, List<Integer>> userInfluencersAuthorityScores;

	public GetNewSeeds(Map<Long, List<Integer>> scores, int scoreThreshold, double percentageThreshold) {
		if (scores == null || scores.size() == 0) {
			throw new RuntimeException("The map is null or empty");
		}
		this.userInfluencersAuthorityScores = scores;
		this.scoreThreshold = scoreThreshold;
		this.percentageThreshold = percentageThreshold;
	}
	
	public List<Pair<Long, Double>> getAuthorityScorePercentage() {
		List<Pair<Long, Double>> result = new ArrayList<Pair<Long, Double>>();
		for(Long key : this.userInfluencersAuthorityScores.keySet()) {
			List<Integer> authorityScores = this.userInfluencersAuthorityScores.get(key);
			int count = 0;
			for(Integer key2 : authorityScores) {
				if (key2 >= scoreThreshold) {
					count++;
				}
			}
			result.add(new Pair<Long, Double>(key, (double) count / authorityScores.size()));
		}
		return result;
	}
		
	public List<Long> getExpandedSeeds() {
		List<Pair<Long, Double>> scores = this.getAuthorityScorePercentage();
		List<Long> result = new ArrayList<Long>();
		for(Pair<Long, Double> keys : scores) {
			if (keys.getB() >= this.percentageThreshold) {
				result.add(keys.getA());
			}
		}
		return result;
	}
}