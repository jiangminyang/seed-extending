package com.sysomos.core.location_engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sysomos.core.location_engine.compute.GetNewSeeds;
import com.sysomos.core.location_engine.data.GetInfluencers;
import com.sysomos.user.authority.AuthorityScoreService;
import com.sysomos.user.authority.GetUserAuthorityScore;
import com.sysomos.user.authority.exception.AuthorityScoreServiceException;

public class SeedExtending {
	
	private static final int ScoreThreshold = 8;
	private static final double PercentageThreshold = 0.5;
	
	private final Map<Long, List<Long>> seeds;
	private int scoreThreshold;
	private double percentageThreshold;
	public SeedExtending(String filename) {
		seeds = GetInfluencers.getData(filename);
		this.scoreThreshold = ScoreThreshold;
		this.percentageThreshold = PercentageThreshold;
	}
	
	public void setScoreThreshold(int scoreThreshold) {
		this.scoreThreshold = scoreThreshold;
	}
	
	public void setPercentageThreshold(double percentageThreshold) {
		this.percentageThreshold = percentageThreshold;
	}
	
	public List<Long> compute() throws AuthorityScoreServiceException {
		
		List<Long> IDs = new ArrayList<Long>();
		for(Long keys :seeds.keySet()) {
			for(Long keys2 : seeds.get(keys)) {
				IDs.add(keys2);
			}
		}
		
		GetUserAuthorityScore.ResultContext results = AuthorityScoreService.newGetUserAuthorityScoreRequest(IDs).call();
		
		Map<Long, List<Integer>> influencersAuthorityTable = new HashMap<Long, List<Integer>>();
		for(Long keys : seeds.keySet()) {
			List<Integer> temp = new ArrayList<Integer>();
			for(Long keys2 : seeds.get(keys)) {
				temp.add(results.get(keys2));
			}
			influencersAuthorityTable.put(keys, temp);
		}
		return new GetNewSeeds(influencersAuthorityTable, scoreThreshold, percentageThreshold).getExpandedSeeds();
	}
}