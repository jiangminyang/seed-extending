package com.sysomos.core.location_engine.data;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class Neighbours {

	private static final char SEP = 0x1f;

	public Long key;
	public Double latitude, longitude;
	public String location;

	public Neighbours(String s) {
		String[] strArr = s.split("\t");
		if (strArr != null) {
			key = Long.parseLong(strArr[0].substring(strArr[0].indexOf(":") + 2));
			latitude = strArr[1].split(String.valueOf(SEP))[1] == "" ? null
					: Double.parseDouble(strArr[1].split(String.valueOf(SEP))[1]);
			longitude = strArr[2].split(String.valueOf(SEP))[1] == "" ? null
					: Double.parseDouble(strArr[2].split(String.valueOf(SEP))[1]);
			location = strArr[3].split(String.valueOf(SEP))[1] == "" ? null : strArr[3].split(String.valueOf(SEP))[1];
		}
	}
}

public class GetInfluencers {
	private static final Logger LOG = LoggerFactory.getLogger(GetInfluencers.class);

	public static Map<Long, List<Long>> getData(String file) {
		JSONParser parser = new JSONParser();
		Map<Long, List<Long>> result = new HashMap<Long, List<Long>>();
		try {
			Object obj = parser.parse(new FileReader(file));
			JSONObject jsonObject = (JSONObject) obj;
			for (Object key : jsonObject.keySet()) {
				String userId = (String) key;
				JSONObject user = (JSONObject) jsonObject.get(userId);
				JSONObject userInfo = (JSONObject) user.get("mode");
				JSONObject neighbours = (JSONObject) user.get("neighbours");
				for (Object key2 : neighbours.keySet()) {
					Neighbours neighbour = new Neighbours((String) key2);
					if (neighbour.location.toLowerCase().contains(((String) userInfo.get("location")).toLowerCase())
							|| ((String) userInfo.get("location")).toLowerCase()
									.contains(neighbour.location.toLowerCase())) {
						if (neighbour.latitude.compareTo((Double) userInfo.get("latitude")) == 0
								&& neighbour.longitude.compareTo((Double) userInfo.get("longitude")) == 0) {
							List<Long> temp;
							if (result.get(Long.parseLong(userId)) == null) {
								temp = new ArrayList<Long>();
							} else {
								temp = result.get(Long.parseLong(userId));
							}
							temp.add(neighbour.key);
							result.put(Long.parseLong(userId), temp);
						}

					}

				}

			}
		} catch (Exception e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
		return result;
	}

}