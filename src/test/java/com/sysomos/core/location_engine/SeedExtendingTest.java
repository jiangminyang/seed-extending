package com.sysomos.core.location_engine;


import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import com.sysomos.user.authority.exception.AuthorityScoreServiceException;

public class SeedExtendingTest {

	private static final String filename = "Inference_20151209025920.csv";
	private SeedExtending instance;
	@Rule
	public TestRule watcher = new TestWatcher() {
		@Override
		protected void starting(Description description) {
			System.out.println("Starting test: " + description.getMethodName());
		}
	};

	@Before
	public void setUp() {
		instance = new SeedExtending(filename);
	}

	@Test
	public void computeTest() throws AuthorityScoreServiceException {
		instance.setScoreThreshold(5);
		instance.setPercentageThreshold(0.5);
		List<Long> seeds = instance.compute();
		assert(seeds != null);
		for(int i = 0; i < seeds.size(); i++) {
			System.out.println(seeds.get(i));
		}
	}
}
